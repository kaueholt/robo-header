import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
// import JsonExcel from 'vue-json-excel'
import '@/icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons'
// import firebasesdk from 'firebase'
// import storePlugin from 'store-plugin'

Vue.config.productionTip = false

// library.add(faTachometerAlt)
// Vue.component('downloadExcel', JsonExcel)
// Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
