import axios from 'axios'
/** @type {BaseService} */
export default class BaseService {
  constructor(api) {
    this.api = `${process.env.BASE_API}${api}`
    this.axios = axios
  }

  /**
   * Listar todos os itens
   */
  find(obj) {
    if (obj) {
      const query = Object.keys(obj)
        .map((k) => `${k}=${encodeURIComponent(obj[k])}`)
        .join('&')
      return this.axios.get(`${this.api}?${query}`)
    } else {
      return this.axios.get(`${this.api}`)
    }
  }

  /**
   * Custom Post
   */
  post(url, obj) {
    return this.axios.post(`${this.api}/${url}`, obj)
  }

  /**
   * Listar todos os itens
   */
  params(obj) {
    const query = Object.keys(obj)
      .map((k) => `${k}=${encodeURIComponent(obj[k])}`)
      .join('&')
    return this.axios.get(`${this.api}/param?${query}`)
  }

  /**
   * Listar todos os itens
   */
  search(obj) {
    const query = Object.keys(obj)
      .map((k) => `${k}=${encodeURIComponent(obj[k])}`)
      .join('&')
    return this.axios.get(`${this.api}/search?${query}`)
  }

  /**
   *  Selecionar o equivalente ao id passado
   * @param {*} id
   */
  findOne(id) {
    return this.axios.get(`${this.api}/${id}`)
  }

  /**
   *  Inserir um novo item
   * @param {*} data
   */
  create(data) {
    return this.axios.post(`${this.api}`, data)
  }

  /**
   *  Atualizar um item
   * @param {*} data
   */
  update(id, data) {
    return this.axios.put(`${this.api}/${id}`, data)
  }

  /**
   * Remover um item
   * @param {*} id
   */
  delete(id) {
    return this.axios.delete(`${this.api}/${id}`)
  }
}
