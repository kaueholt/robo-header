import { crudRoutes } from '@/modules/_common/routes/crudChild'

export default crudRoutes('Tipos de leituras', 'measure', 'stethoscope')
