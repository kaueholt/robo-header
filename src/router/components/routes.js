import Layout from '@/views/layout/Layout'
import product from '@/modules/product/routes'
import device from '@/modules/device/routes'
import deviceType from '@/modules/deviceType/routes'
import measure from '@/modules/measure/routes'

export default {
  path: '/componentes',
  component: Layout,
  redirect: '/device/list',
  name: 'Geral Componentes',
  meta: { title: 'Sensores', icon: 'cpu' },
  children: [
    ...device,
    ...product,
    ...deviceType,
    ...measure
  ]
}
