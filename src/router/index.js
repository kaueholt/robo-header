import Vue from 'vue'
import Router from 'vue-router'
import Layout from '../views/layout/Layout'
import Header from '../components/Header'
import components from './components/routes'
import robo from './robo/routes'
import usuario from './usuario/routes'

Vue.use(Router)

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

/* Layout */

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
export const constantRouterMap = [{
  path: '/login',
  component: () =>
    import('@/views/login'),
  hidden: true
},
{
  path: '/404',
  component: () =>
    import('@/views/404'),
  hidden: true
},
{
  path: '/teste',
  component: Header,
  redirect: '/header',
  meta: { title: 'Header', icon: 'home1' }
},
{
  path: '/',
  component: Layout,
  redirect: '/dashboard',
  name: '33Robotics',
  meta: { title: 'Home', icon: 'home1' },
  // hidden: true,
  children: [{
    path: 'dashboard',
    meta: { title: 'Dashboard', icon: 'pie-chart' },
    component: () =>
      import('@/views/dashboard')
  },
  {
    path: 'info',
    meta: { title: 'Info', icon: 'info' },
    component: () =>
      // import('@/views/dashboard/info')
      import('@/components/Header')
  }
  ]
},
robo,
components,
usuario,
{ path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //Backend support can be opened
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
